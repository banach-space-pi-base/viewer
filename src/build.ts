export const branch = process.env.REACT_APP_BRANCH || 'dev'
export const commitRef = process.env.REACT_APP_COMMIT_REF || 'head'
export const context = process.env.REACT_APP_CONTEXT || 'dev'
export const netlify = process.env.REACT_APP_NETLIFY || false
