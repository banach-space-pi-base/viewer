import { Property, Space, Theorem, Trait } from './models'

export const baseUrl = '/viewer'

export const property = (property: Property) =>
  `${baseUrl}/properties/${property.uid}`

export const space = (space: Space) => `${baseUrl}/spaces/${space.uid}`

export const theorem = (theorem: Theorem) =>
  `${baseUrl}/theorems/${theorem.uid}`

export const trait = ({
  space,
  property,
}: Pick<Trait, 'space' | 'property'>) => {
  return `${baseUrl}/spaces/${space}/properties/${property}`
}

const dataRepo = `https://gitlab.com/banach-space-pi-base/data`
const viewerRepo = `https://gitlab.com/banach-space-pi-base/viewer`

export function contributeExample() {
  return `${dataRepo}`
}

export function resolveId(id: string) {
  switch (id.toLowerCase()[0]) {
    case 't':
    case 'i':
      return `${baseUrl}/theorems/${id}`
    case 'p':
      return `${baseUrl}/properties/${id}`
    case 's':
      return `${baseUrl}/spaces/${id}`
    default:
      // TODO
      throw new Error(`Could not resolve id=${id}`)
  }
}

export function viewerIssues({ body, title }: { body: string; title: string }) {
  return `${viewerRepo}/issues/new?title=${title}&body=${body}`
}

export function contributingGuide() {
  return `${dataRepo}/blob/master/CONTRIBUTING.md`
}

export default {
  contributeExample,
  contributingGuide,
  property,
  space,
  theorem,
  trait,
  viewerIssues,
  baseUrl,
}
