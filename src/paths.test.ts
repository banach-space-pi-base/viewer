import { resolveId, baseUrl } from './paths'

describe('resolveId', () => {
  it('resolves theorem ids', () => {
    expect(resolveId('T000001')).toEqual(`${baseUrl}/theorems/T000001`)
  })
})
